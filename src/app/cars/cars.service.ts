import { Injectable } from '@angular/core';
import { Observable, of } from "rxjs";
import { Car } from './cars.types';

@Injectable({
  providedIn: 'root'
})
export class CarsService {

  private cars: Car[] = [
    {
      city_mpg: 20,
      class: 'SUV',
      combination_mpg: 18,
      cylinders: 6,
      displacement: 3000,
      drive: 'AWD',
      fuel_type: 'Gasoline',
      highway_mpg: 25,
      make: 'Toyota',
      model: 'Highlander',
      transmission: 'Automatic',
      year: 2022
    },
    {
      city_mpg: 25,
      class: 'Sedan',
      combination_mpg: 28,
      cylinders: 4,
      displacement: 2000,
      drive: 'FWD',
      fuel_type: 'Gasoline',
      highway_mpg: 32,
      make: 'Honda',
      model: 'Accord',
      transmission: 'CVT',
      year: 2022
    },
    {
      city_mpg: 18,
      class: 'Truck',
      combination_mpg: 16,
      cylinders: 8,
      displacement: 5000,
      drive: '4WD',
      fuel_type: 'Gasoline',
      highway_mpg: 22,
      make: 'Ford',
      model: 'F-150',
      transmission: 'Automatic',
      year: 2022
    },
    {
      city_mpg: 30,
      class: 'Hatchback',
      combination_mpg: 34,
      cylinders: 4,
      displacement: 1500,
      drive: 'FWD',
      fuel_type: 'Gasoline',
      highway_mpg: 38,
      make: 'Volkswagen',
      model: 'Golf',
      transmission: 'Automatic',
      year: 2022
    }
  ];

  getCarsInformation(): Observable<Car[]> {
    return of(this.cars);
  }
}
