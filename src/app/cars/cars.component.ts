import { Component, OnInit } from '@angular/core';
import { CarsService } from './cars.service';
import { Car } from './cars.types';


@Component({
  selector: 'app-cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.scss']
})
export class CarsComponent implements OnInit {

  carsInfo: Car[] = [];

  constructor(private carsService: CarsService) { }

  ngOnInit(): void {
    this.carsService.getCarsInformation().subscribe(cars => this.carsInfo = cars);
  }
}
